package br.com.conductor.desafio_conductor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@SpringBootApplication
@EnableAutoConfiguration
@EntityScan(basePackages = {"br.com.conductor.desafio_conductor.entity","br.com.conductor.desafio_conductor.projection"})
@EnableJpaRepositories("br.com.conductor.desafio_conductor.repository")
public class DesafioConductorApplication{

	public static void main(String[] args) {
		SpringApplication.run(DesafioConductorApplication.class, args);
	}

}
