/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.desafio_conductor.controller;

import br.com.conductor.desafio_conductor.entity.Pessoa;
import br.com.conductor.desafio_conductor.entity.Solicitacao;
import br.com.conductor.desafio_conductor.service.SolicitacaoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.annotations.Tag;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Ornélio Hinterholz Junior
 */
@RestController
@Api(tags = {"Solicitação"})
@SwaggerDefinition(tags = @Tag(name = "Solicitação", description = "Manutenção do Cadastro de Solicitações (Concessão de Cartões)"))
@Slf4j
@RequestMapping("api/solicitacao")
public class SolicitacaoController {
    
    @Autowired
    SolicitacaoService solicitacaoService;
    
    private static final Logger logger = LoggerFactory.getLogger(SolicitacaoController.class);
    
    @ApiOperation(value = "Realiza a listagem das Solicitações registradas", 
            notes = "Os filtros disponíveis para a consulta são: cpf, status, tipo, page (nº da página iniciando em 1), size (qtde de registros por página)")
    @GetMapping("/listar")
    public Page<Solicitacao> lista(
            @RequestParam(value = "cpf", required = false, defaultValue = "%") final String cpf,
            @RequestParam(value = "status", required = false, defaultValue = "%") final String status,
            @RequestParam(value = "tipo", required = false, defaultValue = "%") final String tipo,
            @RequestParam(value = "page", required = false, defaultValue = "1") final Integer page,
            @RequestParam(value = "size", required = false, defaultValue = "100") final Integer size) {
        logger.info("Realizando consulta na tabela de pessoas");
                
        return solicitacaoService.findAllProjectedBy(
                cpf,
                status,
                tipo,
                page,
                size);
    }
    
    @ApiOperation(value = "Realiza a Inserção de uma Solicitação")
    @PostMapping("/inserir")
    public ResponseEntity<Solicitacao> insere(@RequestBody Solicitacao solicitacao) {
        try {
            Solicitacao response = solicitacaoService.insere(solicitacao);
            return ResponseEntity.ok(response);
        } catch (Throwable t) {
            return ResponseEntity.unprocessableEntity().build();
        }
    }
    
    @ApiOperation(value = "Realiza a Exclusão de uma Solicitação")
    @DeleteMapping("/excluir")
    public ResponseEntity<Object> exclue(@RequestParam(value = "id", required = true) final Long id) {
        Optional<Solicitacao> optSolicitacao = solicitacaoService.findById(id);
        if (optSolicitacao.isPresent()) {
            solicitacaoService.exclue(optSolicitacao.get());
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    
    @ApiOperation(value = "Realiza a Atualização de uma Solicitação")
    @PutMapping("/atualizar")
    public ResponseEntity<Solicitacao> atualiza(@RequestBody Solicitacao solicitacao) {
        Optional<Solicitacao> optSolicitacao = solicitacaoService.findById(solicitacao.getId());
        if (optSolicitacao.isPresent()) {
            try {
                Solicitacao response = solicitacaoService.atualiza(solicitacao);
                return ResponseEntity.ok(response);
            } catch (Throwable t) {
                return ResponseEntity.unprocessableEntity().build();
            }
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
