/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.desafio_conductor.controller;

import br.com.conductor.desafio_conductor.entity.Pessoa;
import br.com.conductor.desafio_conductor.service.PessoaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.annotations.Tag;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Ornélio Hinterholz Junior
 */
@RestController
@Api(tags = {"Pessoa"})
@SwaggerDefinition(tags = @Tag(name = "Pessoa", description = "Manutenção do Cadastro de Pessoas"))
@Slf4j
@RequestMapping("api/pessoa")
public class PessoaController {

    @Autowired
    PessoaService pessoaService;

    private static final Logger logger = LoggerFactory.getLogger(PessoaController.class);

    @ApiOperation(value = "Realiza a listagem das Pessoas registradas",
            notes = "Os filtros disponíveis para a consulta são: nome, cpf, matricula, page (nº da página iniciando em 1), size (qtde de registros por página)")
    @GetMapping("/listar")
    public Page<Pessoa> lista(
            @RequestParam(value = "nome", required = false, defaultValue = "%") final String nome,
            @RequestParam(value = "cpf", required = false, defaultValue = "%") final String cpf,
            @RequestParam(value = "matricula", required = false, defaultValue = "%") final String matricula,
            @RequestParam(value = "page", required = false, defaultValue = "1") final Integer page,
            @RequestParam(value = "size", required = false, defaultValue = "100") final Integer size) {
        logger.info("Realizando consulta na tabela de pessoas");

        return pessoaService.findAllProjectedBy(
                nome,
                cpf,
                matricula,
                page,
                size);
    }

    @ApiOperation(value = "Realiza a Inserção de uma Pessoa")
    @PostMapping("/inserir")
    public ResponseEntity<Pessoa> insere(@RequestBody Pessoa pessoa) {
        try {
            Pessoa response = pessoaService.insere(pessoa);
            return ResponseEntity.ok(response);
        } catch (Throwable t) {
            return ResponseEntity.unprocessableEntity().build();
        }
    }

    @ApiOperation(value = "Realiza a Exclusão de uma Pessoa")
    @DeleteMapping("/excluir")
    public ResponseEntity<Object> exclue(@RequestParam(value = "id", required = true) final Long id) {
        Optional<Pessoa> optPessoa = pessoaService.findById(id);
        if (optPessoa.isPresent()) {
            pessoaService.exclue(optPessoa.get());
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @ApiOperation(value = "Realiza a Atualização de uma Pessoa")
    @PutMapping("/atualizar")
    public ResponseEntity<Pessoa> atualiza(@RequestBody Pessoa pessoa) {
        Optional<Pessoa> optPessoa = pessoaService.findById(pessoa.getId());
        if (optPessoa.isPresent()) {
            try {
                Pessoa response = pessoaService.atualiza(pessoa);
                return ResponseEntity.ok(response);
            } catch (Throwable t) {
                return ResponseEntity.unprocessableEntity().build();
            }
        } else {
            return ResponseEntity.notFound().build();
        }
    }

}
