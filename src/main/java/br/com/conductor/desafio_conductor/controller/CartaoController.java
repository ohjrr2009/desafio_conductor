/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.desafio_conductor.controller;

import br.com.conductor.desafio_conductor.service.CartaoService;
import br.com.conductor.desafio_conductor.entity.Cartao;
import br.com.conductor.desafio_conductor.entity.Pessoa;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.annotations.Tag;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Ornélio Hinterholz Junior
 */
@RestController
@Api(tags = {"Cartão"})
@SwaggerDefinition(tags = @Tag(name = "Cartão", description = "Manutenção do Cadastro de Cartões"))
@Slf4j
@RequestMapping("api/cartao")
public class CartaoController {
    
    @Autowired
    CartaoService cartaoService;
        
    private static final Logger logger = LoggerFactory.getLogger(CartaoController.class);
    
    @ApiOperation(value = "Realiza a listagem dos Cartões registrados", 
            notes = "Os filtros disponíveis para a consulta são: cpf, numCartao, bandeira, page (nº da página iniciando em 1), size (qtde de registros por página)")
    @GetMapping("/listar")
    public Page<Cartao> lista(
            @RequestParam(value = "cpf", required = false, defaultValue = "%") final String cpf,
            @RequestParam(value = "numCartao", required = false, defaultValue = "%") final String numCartao,
            @RequestParam(value = "bandeira", required = false, defaultValue = "%") final String bandeira,
            @RequestParam(value = "page", required = false, defaultValue = "1") final Integer page,
            @RequestParam(value = "size", required = false, defaultValue = "100") final Integer size) {
        logger.info("Realizando consulta na tabela de Cartões");
                
        return cartaoService.findAllProjectedBy(
                cpf,
                numCartao,
                bandeira,
                page,
                size);
    }
    
    @ApiOperation(value = "Realiza a Inserção de um Cartão")
    @PostMapping("/inserir")
    public ResponseEntity<Cartao> insere(@RequestBody Cartao cartao) {
        try {
            Cartao response = cartaoService.insere(cartao);
            return ResponseEntity.ok(response);
        } catch (Throwable t) {
            return ResponseEntity.unprocessableEntity().build();
        }
    }
    
    @ApiOperation(value = "Realiza a Exclusão de um Cartão")
    @DeleteMapping("/excluir")
    public ResponseEntity<Object> exclue(@RequestParam(value = "id", required = true) final Long id) {
        Optional<Cartao> optCartao = cartaoService.findById(id);
        if (optCartao.isPresent()) {
            cartaoService.exclue(optCartao.get());
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    
    @ApiOperation(value = "Realiza a Atualização de um Cartão")
    @PutMapping("/atualizar")
    public ResponseEntity<Cartao> atualiza(@RequestBody Cartao cartao) {
        Optional<Cartao> optCartao = cartaoService.findById(cartao.getId());
        if (optCartao.isPresent()) {
            try {
                Cartao response = cartaoService.atualiza(cartao);
                return ResponseEntity.ok(response);
            } catch (Throwable t) {
                return ResponseEntity.unprocessableEntity().build();
            }
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
