/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.desafio_conductor.controller;

import br.com.conductor.desafio_conductor.entity.Cliente;
import br.com.conductor.desafio_conductor.entity.Pessoa;
import br.com.conductor.desafio_conductor.service.ClienteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.annotations.Tag;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Ornélio Hinterholz Junior
 */
@RestController
@Api(tags = {"Cliente"})
@SwaggerDefinition(tags = @Tag(name = "Cliente", description = "Manutenção do Cadastro de Clientes"))
@Slf4j
@RequestMapping("api/cliente")
public class ClienteController {
    
    @Autowired
    ClienteService clienteService;
    
    private static final Logger logger = LoggerFactory.getLogger(ClienteController.class);
    
    @ApiOperation(value = "Realiza a listagem de Clientes registrados", 
            notes = "Os filtros disponíveis para a consulta são: nome, cpf, page (nº da página iniciando em 1), size (qtde de registros por página)")
    @GetMapping("/listar")
    public Page<Cliente> lista(
            @RequestParam(value = "nome", required = false, defaultValue = "%") final String nome,
            @RequestParam(value = "cpf", required = false, defaultValue = "%") final String cpf,
            @RequestParam(value = "page", required = false, defaultValue = "1") final Integer page,
            @RequestParam(value = "size", required = false, defaultValue = "100") final Integer size) {
        logger.info("Realizando consulta na tabela de Clientes");
        
        return clienteService.findAllProjectedBy(
                nome,
                cpf,
                page,
                size);
    }
    
    @ApiOperation(value = "Realiza a Inserção de um Cliente")
    @PostMapping("/inserir")
    public ResponseEntity<Cliente> insere(@RequestBody Cliente cliente) {
        try {
            Cliente response = clienteService.insere(cliente);
            return ResponseEntity.ok(response);
        } catch (Throwable t) {
            return ResponseEntity.unprocessableEntity().build();
        }
    }
    
    @ApiOperation(value = "Realiza a Exclusão de um Cliente")
    @DeleteMapping("/excluir")
    public ResponseEntity<Object> exclue(@RequestParam(value = "id", required = true) final Long id) {
        Optional<Cliente> optCliente = clienteService.findById(id);
        if (optCliente.isPresent()) {
            clienteService.exclue(optCliente.get());
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    
    @ApiOperation(value = "Realiza a Atualização de um Cliente")
    @PutMapping("/atualizar")
    public ResponseEntity<Cliente> atualiza(@RequestBody Cliente cliente) {
        Optional<Cliente> optCliente = clienteService.findById(cliente.getId());
        if (optCliente.isPresent()) {
            try {
                Cliente response = clienteService.atualiza(cliente);
                return ResponseEntity.ok(response);
            } catch (Throwable t) {
                return ResponseEntity.unprocessableEntity().build();
            }
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
