/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.desafio_conductor.controller;

import br.com.conductor.desafio_conductor.entity.Pessoa;
import br.com.conductor.desafio_conductor.entity.Usuario;
import br.com.conductor.desafio_conductor.service.UsuarioService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.annotations.Tag;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Ornélio Hinterholz Junior
 */
@RestController
@Api(tags = {"Usuário"})
@SwaggerDefinition(tags = @Tag(name = "Usuário", description = "Manutenção do Cadastro de Usuários"))
@Slf4j
@RequestMapping("api/usuario")
public class UsuarioController {
    
    @Autowired
    UsuarioService usuarioService;
    
    private static final Logger logger = LoggerFactory.getLogger(UsuarioController.class);
    
    @ApiOperation(value = "Realiza a listagem dos Usuários registrados", 
            notes = "Os filtros disponíveis para a consulta são: login, ativo, matricula, page (nº da página iniciando em 1), size (qtde de registros por página)")
    @GetMapping("/listar")
    public Page<Usuario> lista(
            @RequestParam(value = "login", required = false, defaultValue = "%") final String nome,
            @RequestParam(value = "ativo", required = false) final Boolean ativo,
            @RequestParam(value = "matricula", required = false, defaultValue = "%") final String matricula,
            @RequestParam(value = "page", required = false, defaultValue = "1") final Integer page,
            @RequestParam(value = "size", required = false, defaultValue = "100") final Integer size) {
        logger.info("Realizando consulta na tabela de Usuários");
        
        return usuarioService.findAllProjectedBy(
                nome,
                ativo,
                matricula,
                page,
                size);
    }
    
    @ApiOperation(value = "Realiza a Inserção de um Usuário")
    @PostMapping("/inserir")
    public ResponseEntity<Usuario> insere(@RequestBody Usuario usuario) {
        try {
            Usuario response = usuarioService.insere(usuario);
            return ResponseEntity.ok(response);
        } catch (Throwable t) {
            return ResponseEntity.unprocessableEntity().build();
        }
    }
    
    @ApiOperation(value = "Realiza a Exclusão de um Usuário")
    @DeleteMapping("/excluir")
    public ResponseEntity<Object> exclue(@RequestParam(value = "id", required = true) final Long id) {
        Optional<Usuario> optUsuario = usuarioService.findById(id);
        if (optUsuario.isPresent()) {
            usuarioService.exclue(optUsuario.get());
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    
    @ApiOperation(value = "Realiza a Atualização de um Usuário")
    @PutMapping("/atualizar")
    public ResponseEntity<Usuario> atualiza(@RequestBody Usuario usuario) {
        Optional<Usuario> optUsuario = usuarioService.findById(usuario.getId());
        if (optUsuario.isPresent()) {
            try {
                Usuario response = usuarioService.atualiza(usuario);
                return ResponseEntity.ok(response);
            } catch (Throwable t) {
                return ResponseEntity.unprocessableEntity().build();
            }
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
