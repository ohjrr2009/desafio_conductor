package br.com.conductor.desafio_conductor.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.NegatedRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private static final RequestMatcher PUBLIC_URLS = new OrRequestMatcher(
            new AntPathRequestMatcher("/health/**"),
            new AntPathRequestMatcher("/info/**"),
            new AntPathRequestMatcher("/actuator/**"),
            new AntPathRequestMatcher("/"),
            new AntPathRequestMatcher("/v2/api-docs"),
            new AntPathRequestMatcher("/swagger-ui.html")
    );
    private static final RequestMatcher PROTECTED_URLS = new NegatedRequestMatcher(PUBLIC_URLS);

    @Override
    public void configure(final HttpSecurity http) throws Exception {
        http
            .csrf().disable() //habilitar a posteriori quando houver mecanismo de criação de Tokens de autenticação
            .authorizeRequests()
                .antMatchers("/", "/v2/api-docs", "/swagger-ui.html").hasRole("ADMIN")
                .antMatchers("/health*", "/info*", "/actuator*").permitAll()
                .antMatchers("/login*").authenticated()
            .and()
            .formLogin()
                .loginProcessingUrl("/login")
                .defaultSuccessUrl("/")
                .usernameParameter("user_login")
                .passwordParameter("user_password")
            .and()
            .logout()
                .logoutUrl("/logout")
                .logoutSuccessUrl("/login")
                .permitAll()
            .and()
            .sessionManagement()
                .invalidSessionUrl("/login?time=30")
                .maximumSessions(10);
    }
    
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .inMemoryAuthentication()
                .withUser("admin").password(passwordEncoder().encode("admin")).roles("ADMIN")
                .and()
                .withUser("operario").password(passwordEncoder().encode("operario")).roles("OPERARIO")
                .and()
                .withUser("analista").password(passwordEncoder().encode("analista")).roles("ANALISTA");
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
