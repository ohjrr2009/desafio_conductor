package br.com.conductor.desafio_conductor.config;

import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import springfox.documentation.service.Contact;
import org.springframework.boot.info.BuildProperties;


@Configuration
@EnableSwagger2
public class SwaggerConfig implements WebMvcConfigurer {
   
    @Autowired
    BuildProperties buildProperties;

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("br.com.conductor.desafio_conductor.controller"))
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build()
                .apiInfo(metaInfo());
    }

    private ApiInfo metaInfo() {

        String title = "ANÁLISE DE CRÉDITO - API BACK";
        String description = "API for Conductor";
        String version = buildProperties.getVersion();
        String termsOfServiceUrl = "Terms of Service";
        Contact contact = new Contact("Conductor", "www.conductor.com.br", "atendimento@conductor.com.br");
        String license = "Conductor";
        String licenseUrl = "https://www.conductor.com.br";

        return new ApiInfo(title, description, version, termsOfServiceUrl, contact, license, licenseUrl, new ArrayList<>());
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addRedirectViewController("/", "swagger-ui.html");
    }
}
