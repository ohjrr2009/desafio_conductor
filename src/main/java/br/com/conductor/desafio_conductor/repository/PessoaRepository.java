/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.desafio_conductor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import br.com.conductor.desafio_conductor.entity.Pessoa;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author Ornélio Hinterholz Junior
 */
@Repository
public interface PessoaRepository extends JpaRepository<Pessoa, Long>{
    
    Pessoa findByCpf(String cpf);

    default Page<Pessoa> findAllProjectedBy(String nome, String cpf, String matricula, Pageable pageable){
        return findAllProjectedByNomeLikeAndCpfLikeAndMatriculaLikeOrderByNomeAsc(
                    nome, cpf, matricula, pageable
                );
    }
    
    Page<Pessoa> findAllProjectedByNomeLikeAndCpfLikeAndMatriculaLikeOrderByNomeAsc(String nome, String cpf, String matricula, Pageable pageable);    
}
