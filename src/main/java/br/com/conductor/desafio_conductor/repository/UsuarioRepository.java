/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.desafio_conductor.repository;

import br.com.conductor.desafio_conductor.entity.Pessoa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import br.com.conductor.desafio_conductor.entity.Usuario;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author Ornélio Hinterholz Junior
 */
@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long>{

    default Page<Usuario> findAllProjectedBy(String login, List<Boolean> ativo, String matricula, Pageable pageable){
        return findAllProjectedByLoginLikeAndAtivoInAndPessoa_MatriculaLikeOrderByLoginDesc(login, ativo, matricula, pageable);
    }
    
    Page<Usuario> findAllProjectedByLoginLikeAndAtivoInAndPessoa_MatriculaLikeOrderByLoginDesc(String login, List<Boolean> ativo, String matricula, Pageable pageable);
    
}
