/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.desafio_conductor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import br.com.conductor.desafio_conductor.entity.UsuarioPerfil;

/**
 *
 * @author Ornélio Hinterholz Junior
 */
@Repository
public interface UsuarioPerfilRepository extends JpaRepository<UsuarioPerfil, Long>{
    
}
