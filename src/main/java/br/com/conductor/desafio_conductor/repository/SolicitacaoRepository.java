/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.desafio_conductor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import br.com.conductor.desafio_conductor.entity.Solicitacao;
import br.com.conductor.desafio_conductor.entity.Cliente;
import br.com.conductor.desafio_conductor.entity.StatusSolicitacao;
import br.com.conductor.desafio_conductor.entity.TipoSolicitacao;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author Ornélio Hinterholz Junior
 */
@Repository
public interface SolicitacaoRepository extends JpaRepository<Solicitacao, Long>{
    
    default Page<Solicitacao> findAllProjectedBy(String cpf, String status, String tipo, Pageable pageable){
        return findAllProjectedByCliente_CpfLikeAndStatus_NomeLikeAndTipoSolicitacao_NomeLikeOrderByUpdatedAtAsc(cpf, status, tipo, pageable);
    }
    
    Page<Solicitacao> findAllProjectedByCliente_CpfLikeAndStatus_NomeLikeAndTipoSolicitacao_NomeLikeOrderByUpdatedAtAsc(String cpf, String status, String tipo, Pageable pageable);

    
}
