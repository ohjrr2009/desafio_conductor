/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.desafio_conductor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import br.com.conductor.desafio_conductor.entity.Cartao;
import br.com.conductor.desafio_conductor.entity.Cliente;
import java.util.Date;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author Ornélio Hinterholz Junior
 */
@Repository
public interface CartaoRepository extends JpaRepository<Cartao, Long>{
    
    default Page<Cartao> findAllProjectedBy(String cpf, String numCartao, String bandeira, Pageable pageable){
        return findAllProjectedByCliente_CpfLikeAndNumCartaoLikeAndBandeiraLikeOrderByDtValidadeAsc(cpf, numCartao, bandeira, pageable);
    }
    
    Page<Cartao> findAllProjectedByCliente_CpfLikeAndNumCartaoLikeAndBandeiraLikeOrderByDtValidadeAsc(String cpf, String numCartao, String bandeira, Pageable pageable);
    
}
