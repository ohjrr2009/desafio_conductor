/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.desafio_conductor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import br.com.conductor.desafio_conductor.entity.Cliente;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author Ornélio Hinterholz Junior
 */
@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long>{
    
    Cliente findByCpf(String cpf);
    
    default Page<Cliente> findAllProjectedBy(String nome, String cpf, Pageable pageable){
        return findAllProjectedByNomeLikeAndCpfLikeOrderByNomeAsc(nome, cpf, pageable);
    }
    
    Page<Cliente> findAllProjectedByNomeLikeAndCpfLikeOrderByNomeAsc(String nome, String cpf, Pageable pageable);

    
}
