/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.desafio_conductor.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.UpdateTimestamp;

/**
 *
 * @author Ornélio Hinterholz Junior
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "cartao")
public class Cartao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @NotNull
    @Column(name = "num_cartao", unique = true)
    private String numCartao;
    
    @NotNull
    @Column(name = "nome_impressao")
    private String nomeImpressao;
    
    @NotNull
    private String bandeira;
    
    @NotNull
    private Integer cvc;
    
    @NotNull
    @Column(name = "dt_validade")
    @Temporal(TemporalType.DATE)
    private Date dtValidade;
    
    @NotNull
    @Column(name = "limite_credito")
    private Float limiteCredito;
    
    @NotNull
    private Boolean ativo;

    @NotNull
    @Column(name = "created_at")
    @CreationTimestamp
    private Date createdAt;
    
    @NotNull
    @Column(name = "updated_at")
    @UpdateTimestamp
    private Date updatedAt;
    
    @JsonBackReference(value="cartao-cliente")
    @JoinColumn(name = "cod_cliente", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Cliente cliente;
}
