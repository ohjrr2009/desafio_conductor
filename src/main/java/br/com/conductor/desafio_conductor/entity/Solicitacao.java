/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.desafio_conductor.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.UpdateTimestamp;

/**
 *
 * @author Ornélio Hinterholz Junior
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "solicitacao")
public class Solicitacao {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    private Float limiteCredito;
    
    @NotNull
    @Column(name = "created_at")
    @CreationTimestamp
    private Date createdAt;
    
    @NotNull
    @Column(name = "updated_at")
    @UpdateTimestamp
    private Date updatedAt;
    
    @JsonBackReference(value="solicitacao-cliente")
    @JoinColumn(name = "cod_cliente", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Cliente cliente;
    
    @JsonBackReference(value="solicitacao-status")
    @JoinColumn(name = "cod_status_solicitacao", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private StatusSolicitacao status;
    
    @JsonBackReference(value="solicitacao-tipoSolicitacao")
    @JoinColumn(name = "cod_tipo_solicitacao", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private TipoSolicitacao tipoSolicitacao;
}
