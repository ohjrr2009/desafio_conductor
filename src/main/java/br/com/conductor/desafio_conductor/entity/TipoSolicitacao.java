/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.desafio_conductor.entity;

import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.UpdateTimestamp;

/**
 *
 * @author Ornélio Hinterholz Junior
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "tipo_solicitacao")
public class TipoSolicitacao {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @NotNull
    @Column(unique = true)
    private String nome;
    
    private String descricao;
    
    @NotNull
    @Column(name = "created_at")
    @CreationTimestamp
    private Date createdAt;
    
    @NotNull
    @Column(name = "updated_at")
    @UpdateTimestamp
    private Date updatedAt;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tipoSolicitacao", fetch = FetchType.LAZY)
    @Fetch(org.hibernate.annotations.FetchMode.SUBSELECT)
    private List<Solicitacao> solicitacaoList;
}
