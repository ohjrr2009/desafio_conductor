/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.desafio_conductor.entity;

import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.UpdateTimestamp;

/**
 *
 * @author Ornélio Hinterholz Junior
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "pessoa")
public class Pessoa {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @NotNull
    private String nome;
    
    private String telefone;
    
    private String celular;
    
    @NotNull
    @Column(unique = true)
    private String cpf;
    
    @NotNull
    private String endereco;
    
    @NotNull
    private String cep;
    
    @NotNull
    @Column(unique = true)
    private String matricula;
    
    @NotNull
    @Column(name = "created_at")
    @CreationTimestamp
    private Date createdAt;
    
    @NotNull
    @Column(name = "updated_at")
    @UpdateTimestamp
    private Date updatedAt;
    
    @Basic(optional = true)
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "pessoa", fetch = FetchType.LAZY)
    private Usuario usuario;   
}
