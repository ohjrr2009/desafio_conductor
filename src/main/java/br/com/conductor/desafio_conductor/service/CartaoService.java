/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.desafio_conductor.service;

import java.util.List;
import java.util.Optional;

import br.com.conductor.desafio_conductor.entity.Cartao;
import java.util.Date;
import org.springframework.data.domain.Page;

/**
 *
 * @author Ornélio Hinterholz Junior
 */
public interface CartaoService {
    Optional<List<Cartao>> findAll();

    Optional<Cartao> findById(Long id);
    
    Page<Cartao> findAllProjectedBy(
            String cpf,
            String numCartao,
            String bandeira,
            Integer page,
            Integer size
    );
    
    Cartao insere(Cartao cartao);
    
    Cartao atualiza(Cartao cartao);
    
    void exclue(Cartao cartao);
}
