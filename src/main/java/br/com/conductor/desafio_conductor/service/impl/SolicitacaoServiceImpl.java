/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.desafio_conductor.service.impl;

import br.com.conductor.desafio_conductor.entity.Solicitacao;
import br.com.conductor.desafio_conductor.repository.SolicitacaoRepository;
import br.com.conductor.desafio_conductor.service.SolicitacaoService;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 *
 * @author Ornélio Hinterholz Junior
 */
@Service
public class SolicitacaoServiceImpl implements SolicitacaoService {

    @Autowired
    SolicitacaoRepository solicitacaoRepository;

    @Override
    public Optional<List<Solicitacao>> findAll() {
        return Optional.of(solicitacaoRepository.findAll());
    }

    @Override
    public Optional<Solicitacao> findById(Long id) {
        return solicitacaoRepository.findById(id);
    }

    @Override
    public Page<Solicitacao> findAllProjectedBy(String cpf, String status, String tipo, Integer page, Integer size) {
        page = page - 1;
        Integer tmpPage = page;
        Integer tmpSize = size;
        Pageable pageable = PageRequest.of(tmpPage, tmpSize);

        return solicitacaoRepository.findAllProjectedBy(
                cpf,
                status,
                tipo,
                pageable);
    }

    @Override
    public Solicitacao insere(Solicitacao solicitacao) {
        return solicitacaoRepository.saveAndFlush(solicitacao);
    }

    @Override
    public Solicitacao atualiza(Solicitacao tmpSolicitacao) {
        Optional<Solicitacao> optSolicitacao = solicitacaoRepository.findById(tmpSolicitacao.getId());
        if (optSolicitacao.isPresent() && !optSolicitacao.get().equals(tmpSolicitacao)) {
            Solicitacao solicitacao = optSolicitacao.get();

            if (tmpSolicitacao.getCliente() != null) {
                solicitacao.setCliente(tmpSolicitacao.getCliente());
            } else if (tmpSolicitacao.getStatus() != null) {
                solicitacao.setStatus(tmpSolicitacao.getStatus());
            } else if (tmpSolicitacao.getTipoSolicitacao() != null) {
                solicitacao.setTipoSolicitacao(tmpSolicitacao.getTipoSolicitacao());
            } else if (tmpSolicitacao.getLimiteCredito() != null) {
                solicitacao.setLimiteCredito(tmpSolicitacao.getLimiteCredito());
            }
            return solicitacaoRepository.saveAndFlush(solicitacao);
        } else {
            return null;
        }
    }

    @Override
    public void exclue(Solicitacao solicitacao) {
        solicitacaoRepository.deleteById(solicitacao.getId());
    }
}
