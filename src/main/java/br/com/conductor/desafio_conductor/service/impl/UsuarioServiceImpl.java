/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.desafio_conductor.service.impl;

import br.com.conductor.desafio_conductor.entity.Usuario;
import br.com.conductor.desafio_conductor.repository.PessoaRepository;
import br.com.conductor.desafio_conductor.repository.UsuarioRepository;
import br.com.conductor.desafio_conductor.service.PessoaService;
import br.com.conductor.desafio_conductor.service.UsuarioService;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 *
 * @author Ornélio Hinterholz Junior
 */
@Service
public class UsuarioServiceImpl implements UsuarioService {

    @Autowired
    UsuarioRepository usuarioRepository;

    @Override
    public Optional<List<Usuario>> findAll() {
        return Optional.of(usuarioRepository.findAll());
    }

    @Override
    public Optional<Usuario> findById(Long id) {
        return usuarioRepository.findById(id);
    }

    @Override
    public Page<Usuario> findAllProjectedBy(String login, Boolean ativo, String matricula, Integer page, Integer size) {
        page = page - 1;
        Integer tmpPage = page;
        Integer tmpSize = size;
        Pageable pageable = PageRequest.of(tmpPage, tmpSize);
        
        List<Boolean> tmpAtivoFlag;
        if (ativo == null) {
            tmpAtivoFlag = Arrays.asList(true, false);
        } else {
            tmpAtivoFlag = Arrays.asList(ativo);
        }

        return usuarioRepository.findAllProjectedBy(
                login,
                tmpAtivoFlag,
                matricula,
                pageable);
    }

    @Override
    public Usuario insere(Usuario usuario) {
        return usuarioRepository.saveAndFlush(usuario);
    }

    @Override
    public Usuario atualiza(Usuario tmpUsuario) {
        Optional<Usuario> optUsuario = usuarioRepository.findById(tmpUsuario.getId());
        if (optUsuario.isPresent() && !optUsuario.get().equals(tmpUsuario)) {
            Usuario usuario = optUsuario.get();
            
            if (tmpUsuario.getLogin() != null && !tmpUsuario.getLogin().isBlank()) {
                usuario.setLogin(tmpUsuario.getLogin());
            } else if (tmpUsuario.getPassword() != null && !tmpUsuario.getPassword().isBlank()) {
                usuario.setPassword(tmpUsuario.getPassword());
            } else if (tmpUsuario.getPessoa()!= null) {
                usuario.setPessoa(tmpUsuario.getPessoa());
            } else if (tmpUsuario.getAtivo()!= null) {
                usuario.setAtivo(tmpUsuario.getAtivo());
            }
            
            return usuarioRepository.saveAndFlush(usuario);
        }
        else{
            return null;
        }
    }

    @Override
    public void exclue(Usuario usuario) {
        usuarioRepository.deleteById(usuario.getId());
    }
}
