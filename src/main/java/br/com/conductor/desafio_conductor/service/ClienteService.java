/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.desafio_conductor.service;

import java.util.List;
import java.util.Optional;

import br.com.conductor.desafio_conductor.entity.Cliente;
import org.springframework.data.domain.Page;

/**
 *
 * @author Ornélio Hinterholz Junior
 */
public interface ClienteService {
    Optional<List<Cliente>> findAll();

    Optional<Cliente> findById(Long id);

    Page<Cliente> findAllProjectedBy(
            String nome,
            String cpf,
            Integer page,
            Integer size
    );
    
    Cliente insere(Cliente cliente);
    
    Cliente atualiza(Cliente cliente);
    
    void exclue(Cliente cliente);
}
