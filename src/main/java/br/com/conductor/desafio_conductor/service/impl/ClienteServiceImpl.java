/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.desafio_conductor.service.impl;

import br.com.conductor.desafio_conductor.entity.Cliente;
import br.com.conductor.desafio_conductor.repository.ClienteRepository;
import br.com.conductor.desafio_conductor.service.ClienteService;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 *
 * @author Ornélio Hinterholz Junior
 */
@Service
public class ClienteServiceImpl implements ClienteService {

    @Autowired
    ClienteRepository clienteRepository;

    @Override
    public Optional<List<Cliente>> findAll() {
        return Optional.of(clienteRepository.findAll());
    }

    @Override
    public Optional<Cliente> findById(Long id) {
        return clienteRepository.findById(id);
    }

    @Override
    public Page<Cliente> findAllProjectedBy(String nome, String cpf, Integer page, Integer size) {
        page = page - 1;
        Integer tmpPage = page;
        Integer tmpSize = size;
        Pageable pageable = PageRequest.of(tmpPage, tmpSize);

        return clienteRepository.findAllProjectedBy(
                nome,
                cpf,
                pageable);
    }

    @Override
    public Cliente insere(Cliente cliente) {
        return clienteRepository.saveAndFlush(cliente);
    }

    @Override
    public Cliente atualiza(Cliente tmpCliente) {
        Optional<Cliente> optCliente = clienteRepository.findById(tmpCliente.getId());
        if (optCliente.isPresent() && !optCliente.get().equals(tmpCliente)) {
            Cliente cliente = optCliente.get();

            if (tmpCliente.getNome() != null && !tmpCliente.getNome().isBlank()) {
                cliente.setNome(tmpCliente.getNome());
            } else if (tmpCliente.getTelefone() != null && !tmpCliente.getTelefone().isBlank()) {
                cliente.setTelefone(tmpCliente.getTelefone());
            } else if (tmpCliente.getCelular() != null && !tmpCliente.getCelular().isBlank()) {
                cliente.setCelular(tmpCliente.getCelular());
            } else if (tmpCliente.getCpf() != null && !tmpCliente.getCpf().isBlank()) {
                cliente.setCpf(tmpCliente.getCpf());
            } else if (tmpCliente.getEndereco() != null && !tmpCliente.getEndereco().isBlank()) {
                cliente.setEndereco(tmpCliente.getEndereco());
            } else if (tmpCliente.getCep() != null && !tmpCliente.getCep().isBlank()) {
                cliente.setCep(tmpCliente.getCep());
            } else if (tmpCliente.getRg() != null) {
                cliente.setRg(tmpCliente.getRg());
            } else if (tmpCliente.getCnh() != null) {
                cliente.setCnh(tmpCliente.getCnh());
            } else if (tmpCliente.getComprovResidencia() != null) {
                cliente.setComprovResidencia(tmpCliente.getComprovResidencia());
            }
            return clienteRepository.saveAndFlush(cliente);
        } else {
            return null;
        }
    }

    @Override
    public void exclue(Cliente cliente) {
        clienteRepository.deleteById(cliente.getId());
    }
}
