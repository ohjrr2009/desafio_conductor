/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.desafio_conductor.service;

import java.util.List;
import java.util.Optional;

import br.com.conductor.desafio_conductor.entity.Usuario;
import org.springframework.data.domain.Page;

/**
 *
 * @author Ornélio Hinterholz Junior
 */
public interface UsuarioService {
    Optional<List<Usuario>> findAll();

    Optional<Usuario> findById(Long id);

    Page<Usuario> findAllProjectedBy(String login, Boolean ativo, String matricula, Integer page, Integer size);
    
    Usuario insere(Usuario usuario);
    
    Usuario atualiza(Usuario usuario);
    
    void exclue(Usuario usuario);
}
