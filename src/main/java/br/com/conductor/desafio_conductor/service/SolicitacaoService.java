/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.desafio_conductor.service;

import java.util.List;
import java.util.Optional;

import br.com.conductor.desafio_conductor.entity.Solicitacao;
import org.springframework.data.domain.Page;

/**
 *
 * @author Ornélio Hinterholz Junior
 */
public interface SolicitacaoService {
    Optional<List<Solicitacao>> findAll();

    Optional<Solicitacao> findById(Long id);

    Page<Solicitacao> findAllProjectedBy(
            String cpf, 
            String status, 
            String tipo,
            Integer page,
            Integer size
    );
    
    Solicitacao insere(Solicitacao solicitacao);
    
    Solicitacao atualiza(Solicitacao solicitacao);
    
    void exclue(Solicitacao solicitacao);
}
