/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.desafio_conductor.service.impl;

import br.com.conductor.desafio_conductor.entity.Cartao;
import br.com.conductor.desafio_conductor.repository.CartaoRepository;
import br.com.conductor.desafio_conductor.service.CartaoService;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 *
 * @author Ornélio Hinterholz Junior
 */
@Service
public class CartaoServiceImpl implements CartaoService {

    @Autowired
    CartaoRepository cartaoRepository;

    @Override
    public Optional<List<Cartao>> findAll() {
        return Optional.of(cartaoRepository.findAll());
    }

    @Override
    public Optional<Cartao> findById(Long id) {
        return cartaoRepository.findById(id);
    }
    
    @Override
    public Page<Cartao> findAllProjectedBy(String cpf, String numCartao, String bandeira, Integer page, Integer size) {
        page = page - 1;
        Integer tmpPage = page;
        Integer tmpSize = size;
        Pageable pageable = PageRequest.of(tmpPage, tmpSize);
        
        return  cartaoRepository.findAllProjectedBy(
                cpf,
                numCartao,
                bandeira,
                pageable
        );
    }

    @Override
    public Cartao insere(Cartao cartao) {
        return cartaoRepository.saveAndFlush(cartao);
    }

    @Override
    public Cartao atualiza(Cartao tmpCartao) {
        Optional<Cartao> optCartao = cartaoRepository.findById(tmpCartao.getId());
        if (optCartao.isPresent() && !optCartao.get().equals(tmpCartao)) {
            Cartao cartao = optCartao.get();
            
            if (tmpCartao.getBandeira() != null && !tmpCartao.getBandeira().isBlank()) {
                cartao.setBandeira(tmpCartao.getBandeira());
            } else if (tmpCartao.getCliente() != null) {
                cartao.setCliente(tmpCartao.getCliente());
            } else if (tmpCartao.getCvc() != null) {
                cartao.setCvc(tmpCartao.getCvc());
            } else if (tmpCartao.getDtValidade() != null) {
                cartao.setDtValidade(tmpCartao.getDtValidade());
            } else if (tmpCartao.getLimiteCredito() != null) {
                cartao.setLimiteCredito(tmpCartao.getLimiteCredito());
            } else if (tmpCartao.getNomeImpressao() != null && !tmpCartao.getNomeImpressao().isBlank()) {
                cartao.setNomeImpressao(tmpCartao.getNomeImpressao());
            } else if (tmpCartao.getNumCartao() != null && !tmpCartao.getNumCartao().isBlank()) {
                cartao.setNumCartao(tmpCartao.getNumCartao());
            }
            return cartaoRepository.saveAndFlush(cartao);
        }
        else{
            return null;
        }
    }

    @Override
    public void exclue(Cartao cartao) {
        cartaoRepository.deleteById(cartao.getId());
    }
}
