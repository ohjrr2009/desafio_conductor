/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.desafio_conductor.service.impl;

import br.com.conductor.desafio_conductor.entity.Pessoa;
import br.com.conductor.desafio_conductor.repository.PessoaRepository;
import br.com.conductor.desafio_conductor.service.PessoaService;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 *
 * @author Ornélio Hinterholz Junior
 */
@Service
public class PessoaServiceImpl implements PessoaService {

    @Autowired
    PessoaRepository pessoaRepository;

    @Override
    public Optional<List<Pessoa>> findAll() {
        return Optional.of(pessoaRepository.findAll());
    }

    @Override
    public Optional<Pessoa> findById(Long id) {
        return pessoaRepository.findById(id);
    }

    @Override
    public Page<Pessoa> findAllProjectedBy(String nome, String cpf, String matricula, Integer page, Integer size) {
        page = page - 1;
        Integer tmpPage = page;
        Integer tmpSize = size;
        Pageable pageable = PageRequest.of(tmpPage, tmpSize);

        return pessoaRepository.findAllProjectedBy(
                nome,
                cpf,
                matricula,
                pageable);
    }

    @Override
    public Pessoa insere(Pessoa pessoa) {
        return pessoaRepository.saveAndFlush(pessoa);
    }

    @Override
    public Pessoa atualiza(Pessoa tmpPessoa) {
        Optional<Pessoa> optPessoa = pessoaRepository.findById(tmpPessoa.getId());
        if (optPessoa.isPresent() && !optPessoa.get().equals(tmpPessoa)) {
            Pessoa pessoa = optPessoa.get();
            
            if (tmpPessoa.getNome() != null && !tmpPessoa.getNome().isBlank()) {
                pessoa.setNome(tmpPessoa.getNome());
            } else if (tmpPessoa.getTelefone()!= null && !tmpPessoa.getTelefone().isBlank()) {
                pessoa.setTelefone(tmpPessoa.getTelefone());
            } else if (tmpPessoa.getCelular()!= null && !tmpPessoa.getCelular().isBlank()) {
                pessoa.setCelular(tmpPessoa.getCelular());
            } else if (tmpPessoa.getCpf()!= null && !tmpPessoa.getCpf().isBlank()) {
                pessoa.setCpf(tmpPessoa.getCpf());
            } else if (tmpPessoa.getEndereco()!= null && !tmpPessoa.getEndereco().isBlank()) {
                pessoa.setEndereco(tmpPessoa.getEndereco());
            } else if (tmpPessoa.getCep()!= null && !tmpPessoa.getCep().isBlank()) {
                pessoa.setCep(tmpPessoa.getCep());
            } else if (tmpPessoa.getMatricula()!= null && !tmpPessoa.getMatricula().isBlank()) {
                pessoa.setMatricula(tmpPessoa.getMatricula());
            }
            return pessoaRepository.saveAndFlush(pessoa);
        }
        else{
            return null;
        }
    }

    @Override
    public void exclue(Pessoa pessoa) {
        pessoaRepository.deleteById(pessoa.getId());
    }
}
