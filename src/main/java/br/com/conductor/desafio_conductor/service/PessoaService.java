/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.desafio_conductor.service;

import java.util.List;
import java.util.Optional;

import br.com.conductor.desafio_conductor.entity.Pessoa;
import org.springframework.data.domain.Page;

/**
 *
 * @author Ornélio Hinterholz Junior
 */
public interface PessoaService {
    Optional<List<Pessoa>> findAll();

    Optional<Pessoa> findById(Long id);

    Page<Pessoa> findAllProjectedBy(
            String nome,
            String cpf,
            String matricula,
            Integer page,
            Integer size
    );
    
    Pessoa insere(Pessoa pessoa);
    
    Pessoa atualiza(Pessoa pessoa);
    
    void exclue(Pessoa pessoa);
}
