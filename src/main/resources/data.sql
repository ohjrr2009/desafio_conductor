--CRIAÇÃO DE PERFIS
INSERT INTO public.perfil(nome, descricao, created_at, updated_at)
	VALUES ('ADM', 'Administrador do Sistema', NOW(), NOW());
INSERT INTO public.perfil(nome, descricao, created_at, updated_at)
	VALUES ('OPR', 'Operador', NOW(), NOW());
INSERT INTO public.perfil(nome, descricao, created_at, updated_at)
	VALUES ('ANA', 'Analista de Crédito', NOW(), NOW());

--CRIAÇÃO DE TIPOS DE SOLICITAÇÕES
INSERT INTO public.tipo_solicitacao(nome, descricao, created_at, updated_at)
	VALUES ('GNCC', 'Pedido de Novo Cartão de Crédito', NOW(), NOW());
INSERT INTO public.tipo_solicitacao(nome, descricao, created_at, updated_at)
	VALUES ('ALCC', 'Pedido de Aumento de Limite de Cartão de Crédito', NOW(), NOW());

--CRIAÇÃO DE STATUS DE SOLICITACÕES
INSERT INTO public.status_solicitacao(nome, descricao, created_at, updated_at)
	VALUES ('GNCC_PENDENTE', 'Pendente de Aprovação de Geração de Novo Cartão de Crédito', NOW(), NOW());
INSERT INTO public.status_solicitacao(nome, descricao, created_at, updated_at)
	VALUES ('GNCC_APROVADO', 'Aprovação de Geração de Novo Cartão de Crédito', NOW(), NOW());
INSERT INTO public.status_solicitacao(nome, descricao, created_at, updated_at)
	VALUES ('GNCC_REJEITADO', 'Rejeição de Geração de Novo Cartão de Crédito', NOW(), NOW());
INSERT INTO public.status_solicitacao(nome, descricao, created_at, updated_at)
	VALUES ('ALCC_PENDENTE', 'Pendente de Aumento de Limite de Cartão de Crédito', NOW(), NOW());
INSERT INTO public.status_solicitacao(nome, descricao, created_at, updated_at)
	VALUES ('ALCC_APROVADO', 'Aprovação de Aumento de Limite de Cartão de Crédito', NOW(), NOW());
INSERT INTO public.status_solicitacao(nome, descricao, created_at, updated_at)
	VALUES ('ALCC_REJEITADO', 'Rejeição de Aumento de Limite de Cartão de Crédito', NOW(), NOW());

--CRIAÇÃO DE PESSOAS
INSERT INTO public.pessoa(matricula, nome, cpf, endereco, cep, created_at, updated_at)
	VALUES ('20003', 'LUMENA DA SILVA', '18592649340', 'RUA L', '04000000', NOW(), NOW());
INSERT INTO public.pessoa(matricula, nome, cpf, endereco, cep, created_at, updated_at)
	VALUES ('20002', 'FIUK JUNIOR', '28450267832', 'RUA F', '05000000', NOW(), NOW());
INSERT INTO public.pessoa(matricula, nome, cpf, endereco, cep, created_at, updated_at)
	VALUES ('20001', 'KAROL CONKA', '05672459812', 'RUA L', '06000000', NOW(), NOW());
INSERT INTO public.pessoa(matricula, nome, cpf, endereco, cep, created_at, updated_at)
	VALUES ('20004', 'SARAH RAMOS', '63349275877', 'RUA S', '07000000', NOW(), NOW());

--CRIAÇÃO DE USUÁRIOS VINCULADOS ÀS PESSOAS
 INSERT INTO public.usuario(login, password, ativo, created_at, updated_at, cod_pessoa)
	VALUES ('lumena', MD5('123'), true, NOW(), NOW(), (SELECT id FROM public.pessoa WHERE cpf = '18592649340'));
 INSERT INTO public.usuario(login, password, ativo, created_at, updated_at, cod_pessoa)
	VALUES ('fiuk', MD5('123'), true, NOW(), NOW(), (SELECT id FROM public.pessoa WHERE cpf = '28450267832'));
 INSERT INTO public.usuario(login, password, ativo, created_at, updated_at, cod_pessoa)
	VALUES ('karol', MD5('123'), true, NOW(), NOW(), (SELECT id FROM public.pessoa WHERE cpf = '05672459812'));
 INSERT INTO public.usuario(login, password, ativo, created_at, updated_at, cod_pessoa)
	VALUES ('sarah', MD5('123'), true, NOW(), NOW(), (SELECT id FROM public.pessoa WHERE cpf = '63349275877'));

--VINCULAÇÃO DE USUÁRIOS AOS PERFIS
INSERT INTO public.usuario_perfil(cod_usuario, cod_perfil, ativo, created_at, updated_at)
	VALUES ((SELECT id FROM public.usuario WHERE login = 'lumena'),
                (SELECT id FROM public.perfil WHERE nome = 'ADM'), true, NOW(), NOW());
INSERT INTO public.usuario_perfil(cod_usuario, cod_perfil, ativo, created_at, updated_at)
	VALUES ((SELECT id FROM public.usuario WHERE login = 'fiuk'),
                (SELECT id FROM public.perfil WHERE nome = 'OPR'), true, NOW(), NOW());
INSERT INTO public.usuario_perfil(cod_usuario, cod_perfil, ativo, created_at, updated_at)
	VALUES ((SELECT id FROM public.usuario WHERE login = 'karol'),
                (SELECT id FROM public.perfil WHERE nome = 'ANA'), true, NOW(), NOW());
INSERT INTO public.usuario_perfil(cod_usuario, cod_perfil, ativo, created_at, updated_at)
	VALUES ((SELECT id FROM public.usuario WHERE login = 'sarah'),
                (SELECT id FROM public.perfil WHERE nome = 'OPR'), true, NOW(), NOW());
INSERT INTO public.usuario_perfil(cod_usuario, cod_perfil, ativo, created_at, updated_at)
	VALUES ((SELECT id FROM public.usuario WHERE login = 'sarah'),
                (SELECT id FROM public.perfil WHERE nome = 'ANA'), true, NOW(), NOW());

--CRIAÇÃO DE CLIENTES
INSERT INTO public.cliente(nome, cpf, endereco, cep, created_at, updated_at)
	VALUES ('NEGO DI', '83762854733', 'RUA N', '98000000', NOW(), NOW());
INSERT INTO public.cliente(nome, cpf, endereco, cep, created_at, updated_at)
	VALUES ('THAIS PEREIRA', '73847295712', 'RUA T', '55000000', NOW(), NOW());
INSERT INTO public.cliente(nome, cpf, endereco, cep, created_at, updated_at)
	VALUES ('GILBERTO MEDEIROS', '38295038574', 'RUA G', '44000000', NOW(), NOW());

--CRIAÇÃO DE SOLICITAÇÕES
INSERT INTO public.solicitacao(cod_cliente, cod_tipo_solicitacao, cod_status_solicitacao, limite_credito, created_at, updated_at)
	VALUES ((SELECT id FROM public.cliente WHERE cpf = '83762854733'),
                (SELECT id FROM public.tipo_solicitacao WHERE nome = 'GNCC'),
                (SELECT id FROM public.status_solicitacao WHERE nome = 'GNCC_PENDENTE'),
                null, NOW(), NOW());

--EVOLUÇÃO DE SOLICITAÇÕES
UPDATE public.solicitacao
	SET cod_status_solicitacao=(SELECT id FROM public.status_solicitacao WHERE nome = 'GNCC_APROVADO'), 
            updated_at=NOW()
	WHERE cod_cliente=(SELECT id FROM public.cliente WHERE cpf = '83762854733') AND
              cod_status_solicitacao=(SELECT id FROM public.status_solicitacao WHERE nome = 'GNCC_PENDENTE');

--CRIAÇÃO DE CARTÕES
INSERT INTO public.cartao(cod_cliente, bandeira, nome_impressao, num_cartao, ativo, cvc, dt_validade, limite_credito, created_at, updated_at)
	VALUES ((SELECT id FROM public.cliente WHERE cpf = '83762854733'),
                 'VISA','NEGO DI','9456912694624027',true,333,'2028-12-31',100.0, NOW(), NOW());

--CRIAÇÃO DE SOLICITAÇÕES
INSERT INTO public.solicitacao(cod_cliente, cod_tipo_solicitacao, cod_status_solicitacao, limite_credito, created_at, updated_at)
	VALUES ((SELECT id FROM public.cliente WHERE cpf = '83762854733'),
                (SELECT id FROM public.tipo_solicitacao WHERE nome = 'ALCC'),
                (SELECT id FROM public.status_solicitacao WHERE nome = 'ALCC_PENDENTE'),
                1000.0, NOW(), NOW());

--EVOLUÇÃO DE SOLICITAÇÕES
UPDATE public.solicitacao
	SET cod_status_solicitacao=(SELECT id FROM public.status_solicitacao WHERE nome = 'ALCC_APROVADO'), 
            updated_at=NOW()
	WHERE cod_cliente=(SELECT id FROM public.cliente WHERE cpf = '83762854733') AND
              cod_status_solicitacao=(SELECT id FROM public.status_solicitacao WHERE nome = 'ALCC_PENDENTE');

