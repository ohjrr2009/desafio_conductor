/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.desafio_conductor.controller;

import br.com.conductor.desafio_conductor.entity.*;
import br.com.conductor.desafio_conductor.entity.Cliente;
import br.com.conductor.desafio_conductor.repository.CartaoRepository;
import br.com.conductor.desafio_conductor.repository.ClienteRepository;
import br.com.conductor.desafio_conductor.service.CartaoService;
import br.com.conductor.desafio_conductor.service.ClienteService;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.jayway.jsonpath.JsonPath;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.util.UriComponentsBuilder;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 *
 * @author Ornélio Hinterholz Junior
 */
@RunWith(SpringRunner.class)
@WebMvcTest(value = ClienteController.class, excludeAutoConfiguration = {SecurityAutoConfiguration.class})
@ContextConfiguration(classes = ClienteController.class)
public class ClienteControllerTest {
    
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ClienteService clienteServiceMock;
    
    @MockBean
    private ClienteRepository clienteRepositoryMock;

    @Mock
    private ResponseEntity responseEntity;

    @Spy
    private Logger logger;

    @Before
    public void init() {
        Mockito.doNothing().when(logger).info("");
        Mockito.doNothing().when(logger).error("");
    }

    @Test
    public void testLista() throws Exception {
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString("/api/cliente/listar");
//                .queryParam("page", "1")
//                .queryParam("size", "2")
//                .queryParam("nome", "%")
//                .queryParam("cpf", "%");
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(uriBuilder.toUriString());

        Cliente cliente = new Cliente();
        cliente.setId(1L);
        cliente.setNome("CLIENTE TESTE");
        cliente.setCpf("83762854733");
        cliente.setEndereco("RUA C");
        cliente.setCep("69300000");

        List<Cliente> list = Arrays.asList(cliente);
        Page<Cliente> page = new PageImpl<>(list);

        when(clienteRepositoryMock.findByCpf("%")).thenReturn(cliente);

        when(clienteRepositoryMock.findAllProjectedBy(
                "%",
                "%",
                PageRequest.of(1,2)
        )).thenReturn(page);

        when(clienteServiceMock.findAllProjectedBy(
                "%",
                "%",
                1,
                2
        )).thenReturn(page);

        MvcResult result = mvc.perform(requestBuilder)
                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.content[0].cpf", is("83762854733")))
                .andReturn();
    }

    @Test
    public void testInsere() throws Exception {
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString("/api/cliente/inserir");

        Cliente cliente = new Cliente();
        cliente.setId(1L);
        cliente.setNome("CLIENTE TESTE");
        cliente.setCpf("83762854733");
        cliente.setEndereco("RUA C");
        cliente.setCep("69300000");

        when(clienteServiceMock.insere(any(Cliente.class))).thenReturn(cliente);

        MvcResult result = mvc.perform(
                MockMvcRequestBuilders.post(uriBuilder.toUriString())
                .content(new JsonMapper().writeValueAsString(cliente))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void testAtualiza() throws Exception {
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString("/api/cliente/atualizar");

        Cliente cliente = new Cliente();
        cliente.setId(1L);
        cliente.setNome("CLIENTE TESTE");
        cliente.setCpf("83762854733");
        cliente.setEndereco("RUA C");
        cliente.setCep("69300000");

        when(clienteServiceMock.findById(any(Long.class))).thenReturn(Optional.of(cliente));

        when(clienteServiceMock.atualiza(any(Cliente.class))).thenReturn(cliente);

        MvcResult result = mvc.perform(
                MockMvcRequestBuilders.put(uriBuilder.toUriString())
                        .content(new JsonMapper().writeValueAsString(cliente))
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void testExclue() throws Exception {
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString("/api/cliente/excluir")
                .queryParam("id", "1");
        RequestBuilder requestBuilder = MockMvcRequestBuilders.delete(uriBuilder.toUriString());

        Cliente cliente = new Cliente();
        cliente.setId(1L);
        cliente.setNome("CLIENTE TESTE");
        cliente.setCpf("83762854733");
        cliente.setEndereco("RUA C");
        cliente.setCep("69300000");

        when(clienteServiceMock.findById(any(Long.class))).thenReturn(Optional.of(cliente));

        when(clienteRepositoryMock.findById(any(Long.class))).thenReturn(Optional.of(cliente));

        clienteServiceMock.exclue(cliente);

        MvcResult result = mvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andReturn();
    }
}
