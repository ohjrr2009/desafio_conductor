/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.desafio_conductor.controller;

import br.com.conductor.desafio_conductor.controller.CartaoController;
import br.com.conductor.desafio_conductor.entity.*;
import br.com.conductor.desafio_conductor.entity.Cartao;
import br.com.conductor.desafio_conductor.entity.Cliente;
import br.com.conductor.desafio_conductor.repository.CartaoRepository;
import br.com.conductor.desafio_conductor.repository.ClienteRepository;
import br.com.conductor.desafio_conductor.service.CartaoService;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.databind.json.JsonMapper;
import com.jayway.jsonpath.JsonPath;
import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.when;
import org.mockito.Spy;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author Ornélio Hinterholz Junior
 */
@RunWith(SpringRunner.class)
@WebMvcTest(value = CartaoController.class, excludeAutoConfiguration = {SecurityAutoConfiguration.class})
@ContextConfiguration(classes = CartaoController.class)
public class CartaoControllerTest {
    
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @Autowired
    private MockMvc mvc;

    @MockBean
    private CartaoService cartaoServiceMock;
    
    @MockBean
    private ClienteRepository clienteRepositoryMock;
    
    @MockBean
    private CartaoRepository cartaoRepositoryMock;

    @Mock
    private ResponseEntity responseEntity;

    @Spy
    private Logger logger;

    @Before
    public void init() {
        Mockito.doNothing().when(logger).info("");
        Mockito.doNothing().when(logger).error("");
    }

    @Test
    public void testLista() throws Exception {
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString("/api/cartao/listar")
                .queryParam("page", "1")
                .queryParam("size", "2")
                .queryParam("cpf", "83762854733");
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(uriBuilder.toUriString());

        Cliente cliente = new Cliente();
        cliente.setId(1L);
        cliente.setNome("CLIENTE TESTE");
        cliente.setCpf("83762854733");
        cliente.setEndereco("RUA C");
        cliente.setCep("69300000");

        Cartao cartao = new Cartao();
        cartao.setId(1L);
        cartao.setNumCartao("8765234567891234");
        cartao.setNomeImpressao("CLIENTE TESTE");
        cartao.setBandeira("VISA");
        cartao.setCvc(333);
        cartao.setDtValidade(sdf.parse("2028-12-31"));
        cartao.setLimiteCredito(1000.0f);
        cartao.setAtivo(true);
        cartao.setCliente(cliente);

        List<Cartao> list = Arrays.asList(cartao);
        Page<Cartao> page = new PageImpl<>(list);

        when(clienteRepositoryMock.findByCpf("83762854733")).thenReturn(cliente);

        when(cartaoRepositoryMock.findAllProjectedBy(
                "83762854733",
                "%",
                "%",
                PageRequest.of(1, 2)
        )).thenReturn(page);

        when(cartaoServiceMock.findAllProjectedBy(
                "83762854733",
                "%",
                "%",
                1,
                2
        )).thenReturn(page);

        MvcResult result = mvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content[0].numCartao", is("8765234567891234")))
                .andReturn();
    }

    @Test
    public void testInsere() throws Exception {
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString("/api/cartao/inserir");

        Cliente cliente = new Cliente();
        cliente.setId(1L);
        cliente.setNome("CLIENTE TESTE");
        cliente.setCpf("83762854733");
        cliente.setEndereco("RUA C");
        cliente.setCep("69300000");

        Cartao cartao = new Cartao();
        cartao.setId(1L);
        cartao.setNumCartao("8765234567891234");
        cartao.setNomeImpressao("CLIENTE TESTE");
        cartao.setBandeira("VISA");
        cartao.setCvc(333);
        cartao.setDtValidade(sdf.parse("2028-12-31"));
        cartao.setLimiteCredito(1000.0f);
        cartao.setAtivo(true);
        cartao.setCliente(cliente);

        when(cartaoServiceMock.insere(any(Cartao.class))).thenReturn(cartao);

        MvcResult result = mvc.perform(
                MockMvcRequestBuilders.post(uriBuilder.toUriString())
                .content(new JsonMapper().writeValueAsString(cartao))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void testAtualiza() throws Exception {
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString("/api/cartao/atualizar");

        Cliente cliente = new Cliente();
        cliente.setId(1L);
        cliente.setNome("CLIENTE TESTE");
        cliente.setCpf("83762854733");
        cliente.setEndereco("RUA C");
        cliente.setCep("69300000");

        Cartao cartao = new Cartao();
        cartao.setId(1L);
        cartao.setNumCartao("8765234567891234");
        cartao.setNomeImpressao("CLIENTE TESTE");
        cartao.setBandeira("VISA");
        cartao.setCvc(333);
        cartao.setDtValidade(sdf.parse("2028-12-31"));
        cartao.setLimiteCredito(1000.0f);
        cartao.setAtivo(true);
        cartao.setCliente(cliente);

        when(cartaoServiceMock.findById(any(Long.class))).thenReturn(Optional.of(cartao));

        when(cartaoServiceMock.atualiza(any(Cartao.class))).thenReturn(cartao);

        MvcResult result = mvc.perform(
                MockMvcRequestBuilders.put(uriBuilder.toUriString())
                        .content(new JsonMapper().writeValueAsString(cartao))
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void testExclue() throws Exception {
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString("/api/cartao/excluir")
                .queryParam("id", "1");
        RequestBuilder requestBuilder = MockMvcRequestBuilders.delete(uriBuilder.toUriString());

        Cartao cartao = new Cartao();
        cartao.setId(1L);
        cartao.setNumCartao("8765234567891234");
        cartao.setNomeImpressao("CLIENTE TESTE");
        cartao.setBandeira("VISA");
        cartao.setCvc(333);
        cartao.setDtValidade(sdf.parse("2028-12-31"));
        cartao.setLimiteCredito(1000.0f);
        cartao.setAtivo(true);

        when(cartaoServiceMock.findById(any(Long.class))).thenReturn(Optional.of(cartao));

        when(cartaoRepositoryMock.findById(any(Long.class))).thenReturn(Optional.of(cartao));

        cartaoServiceMock.exclue(cartao);

        MvcResult result = mvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andReturn();
    }
}
