/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.desafio_conductor.controller;

import br.com.conductor.desafio_conductor.entity.*;
import br.com.conductor.desafio_conductor.entity.Cliente;
import br.com.conductor.desafio_conductor.entity.Pessoa;
import br.com.conductor.desafio_conductor.entity.Usuario;
import br.com.conductor.desafio_conductor.repository.CartaoRepository;
import br.com.conductor.desafio_conductor.repository.ClienteRepository;
import br.com.conductor.desafio_conductor.repository.UsuarioRepository;
import br.com.conductor.desafio_conductor.service.CartaoService;
import br.com.conductor.desafio_conductor.service.UsuarioService;
import com.fasterxml.jackson.databind.json.JsonMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.util.UriComponentsBuilder;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 *
 * @author Ornélio Hinterholz Junior
 */
@RunWith(SpringRunner.class)
@WebMvcTest(value = UsuarioController.class, excludeAutoConfiguration = {SecurityAutoConfiguration.class})
@ContextConfiguration(classes = UsuarioController.class)
public class UsuarioControllerTest {
    
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @Autowired
    private MockMvc mvc;

    @MockBean
    private UsuarioService usuarioServiceMock;
    
    @MockBean
    private UsuarioRepository usuarioRepositoryMock;

    @Mock
    private ResponseEntity responseEntity;

    @Spy
    private Logger logger;

    @Before
    public void init() {
        Mockito.doNothing().when(logger).info("");
        Mockito.doNothing().when(logger).error("");
    }

    @Test
    public void testLista() throws Exception {
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString("/api/usuario/listar");
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(uriBuilder.toUriString());

        Pessoa pessoa = new Pessoa();
        pessoa.setId(1L);
        pessoa.setMatricula("35267");

        Usuario usuario = new Usuario();
        usuario.setId(1L);
        usuario.setLogin("teste");
        usuario.setAtivo(true);
        usuario.setPassword("123");
        usuario.setUsuarioPerfilList(null);
        usuario.setPessoa(pessoa);

        List<Usuario> list = Arrays.asList(usuario);
        Page<Usuario> page = new PageImpl<>(list);

        when(usuarioRepositoryMock.findById(any(Long.class))).thenReturn(Optional.of(usuario));

        when(usuarioRepositoryMock.findAllProjectedBy(
                "teste",
                Arrays.asList(Boolean.TRUE),
                "35267",
                PageRequest.of(1, 2)
        )).thenReturn(page);

        when(usuarioServiceMock.findAllProjectedBy(
                "teste",
                Boolean.TRUE,
                "35267",
                1,
                2
        )).thenReturn(page);

        MvcResult result = mvc.perform(requestBuilder)
                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.content[0].login", is("teste")))
                .andReturn();
    }

    @Test
    public void testInsere() throws Exception {
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString("/api/usuario/inserir");

        Pessoa pessoa = new Pessoa();
        pessoa.setId(1L);
        pessoa.setMatricula("35267");

        Usuario usuario = new Usuario();
        usuario.setId(1L);
        usuario.setLogin("teste");
        usuario.setAtivo(true);
        usuario.setPassword("123");
        usuario.setUsuarioPerfilList(null);
        usuario.setPessoa(pessoa);

        when(usuarioServiceMock.insere(any(Usuario.class))).thenReturn(usuario);

        MvcResult result = mvc.perform(
                MockMvcRequestBuilders.post(uriBuilder.toUriString())
                .content(new JsonMapper().writeValueAsString(usuario))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void testAtualiza() throws Exception {
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString("/api/usuario/atualizar");

        Pessoa pessoa = new Pessoa();
        pessoa.setId(1L);
        pessoa.setMatricula("35267");

        Usuario usuario = new Usuario();
        usuario.setId(1L);
        usuario.setLogin("teste");
        usuario.setAtivo(true);
        usuario.setPassword("123");
        usuario.setUsuarioPerfilList(null);
        usuario.setPessoa(pessoa);

        when(usuarioServiceMock.findById(any(Long.class))).thenReturn(Optional.of(usuario));

        when(usuarioServiceMock.atualiza(any(Usuario.class))).thenReturn(usuario);

        MvcResult result = mvc.perform(
                MockMvcRequestBuilders.put(uriBuilder.toUriString())
                        .content(new JsonMapper().writeValueAsString(usuario))
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void testExclue() throws Exception {
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString("/api/usuario/excluir")
                .queryParam("id", "1");
        RequestBuilder requestBuilder = MockMvcRequestBuilders.delete(uriBuilder.toUriString());

        Pessoa pessoa = new Pessoa();
        pessoa.setId(1L);
        pessoa.setMatricula("35267");

        Usuario usuario = new Usuario();
        usuario.setId(1L);
        usuario.setLogin("teste");
        usuario.setAtivo(true);
        usuario.setPassword("123");
        usuario.setUsuarioPerfilList(null);
        usuario.setPessoa(pessoa);

        when(usuarioServiceMock.findById(any(Long.class))).thenReturn(Optional.of(usuario));

        when(usuarioRepositoryMock.findById(any(Long.class))).thenReturn(Optional.of(usuario));

        usuarioServiceMock.exclue(usuario);

        MvcResult result = mvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andReturn();
    }
}
