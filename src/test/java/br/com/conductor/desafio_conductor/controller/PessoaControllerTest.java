/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.desafio_conductor.controller;

import br.com.conductor.desafio_conductor.entity.*;
import br.com.conductor.desafio_conductor.entity.Cliente;
import br.com.conductor.desafio_conductor.entity.Pessoa;
import br.com.conductor.desafio_conductor.repository.CartaoRepository;
import br.com.conductor.desafio_conductor.repository.ClienteRepository;
import br.com.conductor.desafio_conductor.repository.PessoaRepository;
import br.com.conductor.desafio_conductor.service.CartaoService;
import br.com.conductor.desafio_conductor.service.PessoaService;
import com.fasterxml.jackson.databind.json.JsonMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.util.UriComponentsBuilder;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 *
 * @author Ornélio Hinterholz Junior
 */
@RunWith(SpringRunner.class)
@WebMvcTest(value = PessoaController.class, excludeAutoConfiguration = {SecurityAutoConfiguration.class})
@ContextConfiguration(classes = PessoaController.class)
public class PessoaControllerTest {
    
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @Autowired
    private MockMvc mvc;

    @MockBean
    private PessoaService pessoaServiceMock;
    
    @MockBean
    private PessoaRepository pessoaRepositoryMock;

    @Mock
    private ResponseEntity responseEntity;

    @Spy
    private Logger logger;

    @Before
    public void init() {
        Mockito.doNothing().when(logger).info("");
        Mockito.doNothing().when(logger).error("");
    }

    @Test
    public void testLista() throws Exception {
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString("/api/pessoa/listar");
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(uriBuilder.toUriString());

        Pessoa pessoa = new Pessoa();
        pessoa.setId(1L);
        pessoa.setNome("PESSOA TESTE");
        pessoa.setCpf("83762854733");
        pessoa.setEndereco("RUA C");
        pessoa.setCep("69300000");

        List<Pessoa> list = Arrays.asList(pessoa);
        Page<Pessoa> page = new PageImpl<>(list);

        when(pessoaRepositoryMock.findByCpf("83762854733")).thenReturn(pessoa);

        when(pessoaRepositoryMock.findAllProjectedBy(
                "%",
                "%",
                "%",
                PageRequest.of(1, 2)
        )).thenReturn(page);

        when(pessoaServiceMock.findAllProjectedBy(
                "%",
                "%",
                "%",
                1,
                2
        )).thenReturn(page);

        MvcResult result = mvc.perform(requestBuilder)
                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.content[0].cpf", is("83762854733")))
                .andReturn();
    }

    @Test
    public void testInsere() throws Exception {
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString("/api/pessoa/inserir");

        Pessoa pessoa = new Pessoa();
        pessoa.setId(1L);
        pessoa.setNome("PESSOA TESTE");
        pessoa.setCpf("83762854733");
        pessoa.setEndereco("RUA C");
        pessoa.setCep("69300000");

        when(pessoaServiceMock.insere(any(Pessoa.class))).thenReturn(pessoa);

        MvcResult result = mvc.perform(
                MockMvcRequestBuilders.post(uriBuilder.toUriString())
                .content(new JsonMapper().writeValueAsString(pessoa))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void testAtualiza() throws Exception {
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString("/api/pessoa/atualizar");

        Pessoa pessoa = new Pessoa();
        pessoa.setId(1L);
        pessoa.setNome("PESSOA TESTE");
        pessoa.setCpf("83762854733");
        pessoa.setEndereco("RUA C");
        pessoa.setCep("69300000");

        when(pessoaServiceMock.findById(any(Long.class))).thenReturn(Optional.of(pessoa));

        when(pessoaServiceMock.atualiza(any(Pessoa.class))).thenReturn(pessoa);

        MvcResult result = mvc.perform(
                MockMvcRequestBuilders.put(uriBuilder.toUriString())
                        .content(new JsonMapper().writeValueAsString(pessoa))
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void testExclue() throws Exception {
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString("/api/pessoa/excluir")
                .queryParam("id", "1");
        RequestBuilder requestBuilder = MockMvcRequestBuilders.delete(uriBuilder.toUriString());

        Pessoa pessoa = new Pessoa();
        pessoa.setId(1L);
        pessoa.setNome("PESSOA TESTE");
        pessoa.setCpf("83762854733");
        pessoa.setEndereco("RUA C");
        pessoa.setCep("69300000");

        when(pessoaServiceMock.findById(any(Long.class))).thenReturn(Optional.of(pessoa));

        when(pessoaRepositoryMock.findById(any(Long.class))).thenReturn(Optional.of(pessoa));

        pessoaServiceMock.exclue(pessoa);

        MvcResult result = mvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andReturn();
    }
}
