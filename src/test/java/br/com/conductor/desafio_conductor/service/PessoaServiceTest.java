package br.com.conductor.desafio_conductor.service;

import br.com.conductor.desafio_conductor.entity.*;
import br.com.conductor.desafio_conductor.entity.Pessoa;
import br.com.conductor.desafio_conductor.repository.ClienteRepository;
import br.com.conductor.desafio_conductor.repository.PessoaRepository;
import br.com.conductor.desafio_conductor.service.impl.ClienteServiceImpl;
import br.com.conductor.desafio_conductor.service.impl.PessoaServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;


/**
 * @author Ornélio Hinterholz Junior
 */
@RunWith(SpringRunner.class)
public class PessoaServiceTest {

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @TestConfiguration
    static class PessoaServiceTestContextConfiguration {

        @Bean
        public PessoaService pessoaService() {
            return new PessoaServiceImpl();
        }
    }

    @Autowired
    private PessoaService pessoaService;

    @MockBean
    private PessoaRepository pessoaRepository;

    @MockBean
    RestTemplate restTemplate;

    @Spy
    Logger logger;

    @Before
    public void setUp() throws ParseException {
        Mockito.doNothing().when(logger).info("");
        Mockito.doNothing().when(logger).error("");

        Pessoa pessoa = new Pessoa();
        pessoa.setId(1L);
        pessoa.setNome("PESSOA TESTE");
        pessoa.setCpf("83762854733");
        pessoa.setEndereco("RUA C");
        pessoa.setCep("69300000");

        ResponseEntity<Pessoa> responseEntity = new ResponseEntity<>(pessoa, HttpStatus.OK);

        Mockito.when(restTemplate.postForEntity(any(String.class), any(HttpEntity.class), any(Class.class)))
                .thenReturn(responseEntity);

        Pessoa pessoa2 = new Pessoa();
        pessoa2.setId(1L);
        pessoa2.setNome("CLIENTE TESTE 2");
        pessoa2.setCpf("83762854733");
        pessoa2.setEndereco("RUA C");
        pessoa2.setCep("69300000");

        List<Pessoa> list = Arrays.asList(pessoa);
        Page<Pessoa> page = new PageImpl<>(list);

        Mockito.when(pessoaRepository.findAll())
                .thenReturn(Arrays.asList(pessoa, pessoa2));

        Mockito.when(pessoaRepository.findById(any(Long.class)))
                .thenReturn(Optional.of(pessoa));

        Mockito.when(pessoaRepository.findAllProjectedBy(
                "%",
                "%",
                "%",
                PageRequest.of(1, 2)
        )).thenReturn(page);

        Mockito.when(pessoaService.findAllProjectedBy(
                "%",
                "%",
                "%",
                1,
                2
        )).thenReturn(page);

        Mockito.when(pessoaRepository.saveAndFlush(any(Pessoa.class))).thenReturn(pessoa);
    }

    @Test
    public void testFindById() {
        Optional<Pessoa> found = pessoaService.findById(1L);

        assertThat(found.get()).isEqualTo(
                pessoaRepository.findById(1L).get()
        );
    }

    @Test
    public void testFindByAll() {
        Optional<List<Pessoa>> found = pessoaService.findAll();

        assertThat(found.get()).contains(
                pessoaRepository.findById(1L).get(),
                pessoaRepository.findById(2L).get()
        );
    }

    @Test
    public void testFindAllProjectedBy() {
        Page<Pessoa> found = pessoaService.findAllProjectedBy(
                "%",
                "%",
                "%",
                1,
                2
        );

        assertThat(found.getContent()).contains(
                pessoaRepository.findById(1L).get(),
                pessoaRepository.findById(2L).get()
        );
    }
}
