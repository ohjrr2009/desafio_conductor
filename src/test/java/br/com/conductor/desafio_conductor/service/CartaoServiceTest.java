package br.com.conductor.desafio_conductor.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;

import static org.assertj.core.api.Assertions.assertThat;

import br.com.conductor.desafio_conductor.entity.*;
import br.com.conductor.desafio_conductor.entity.Cliente;
import br.com.conductor.desafio_conductor.repository.CartaoRepository;
import br.com.conductor.desafio_conductor.service.impl.CartaoServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.mockito.ArgumentMatchers.any;

import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;
import org.slf4j.Logger;


/**
 * @author Ornélio Hinterholz Junior
 */
@RunWith(SpringRunner.class)
public class CartaoServiceTest {

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @TestConfiguration
    static class CartaoServiceTestContextConfiguration {

        @Bean
        public CartaoService cartaoService() {
            return new CartaoServiceImpl();
        }
    }

    @Autowired
    private CartaoService cartaoService;

    @MockBean
    private CartaoRepository cartaoRepository;

    @MockBean
    RestTemplate restTemplate;

    @Spy
    Logger logger;

    @Before
    public void setUp() throws ParseException {
        Mockito.doNothing().when(logger).info("");
        Mockito.doNothing().when(logger).error("");

        Cliente cliente = new Cliente();
        cliente.setId(1L);
        cliente.setNome("CLIENTE TESTE");
        cliente.setCpf("83762854733");
        cliente.setEndereco("RUA C");
        cliente.setCep("69300000");

        Cartao cartao = new Cartao();
        cartao.setId(1L);
        cartao.setNumCartao("8765234567891234");
        cartao.setNomeImpressao("CLIENTE TESTE");
        cartao.setBandeira("VISA");
        cartao.setCvc(333);
        cartao.setDtValidade(sdf.parse("2028-12-31"));
        cartao.setLimiteCredito(1000.0f);
        cartao.setAtivo(true);
        cartao.setCliente(cliente);

        ResponseEntity<Cartao> responseEntity = new ResponseEntity<>(cartao, HttpStatus.OK);

        Mockito.when(restTemplate.postForEntity(any(String.class), any(HttpEntity.class), any(Class.class)))
                .thenReturn(responseEntity);

        Cliente cliente2 = new Cliente();
        cliente2.setId(1L);
        cliente2.setNome("CLIENTE TESTE 2");
        cliente2.setCpf("83762854733");
        cliente2.setEndereco("RUA C");
        cliente2.setCep("69300000");

        Cartao cartao2 = new Cartao();
        cartao2.setId(2L);
        cartao2.setNumCartao("9999234567891234");
        cartao2.setNomeImpressao("CLIENTE TESTE 2");
        cartao2.setBandeira("VISA");
        cartao2.setCvc(666);
        cartao2.setDtValidade(sdf.parse("2028-12-31"));
        cartao2.setLimiteCredito(100.0f);
        cartao2.setAtivo(true);
        cartao2.setCliente(cliente2);

        List<Cartao> list = Arrays.asList(cartao);
        Page<Cartao> page = new PageImpl<>(list);

        Mockito.when(cartaoRepository.findAll())
                .thenReturn(Arrays.asList(cartao, cartao2));

        Mockito.when(cartaoRepository.findById(any(Long.class)))
                .thenReturn(Optional.of(cartao));

        Mockito.when(cartaoRepository.findAllProjectedBy(
                "%",
                "%",
                "%",
                PageRequest.of(1, 2)
        )).thenReturn(page);

        Mockito.when(cartaoService.findAllProjectedBy(
                "%",
                "%",
                "%",
                1,
                2
        )).thenReturn(page);

        Mockito.when(cartaoRepository.saveAndFlush(any(Cartao.class))).thenReturn(cartao);
    }

    @Test
    public void testFindById() {
        Optional<Cartao> found = cartaoService.findById(1L);

        assertThat(found.get()).isEqualTo(
                cartaoRepository.findById(1L).get()
        );
    }

    @Test
    public void testFindByAll() {
        Optional<List<Cartao>> found = cartaoService.findAll();

        assertThat(found.get()).contains(
                cartaoRepository.findById(1L).get(),
                cartaoRepository.findById(2L).get()
        );
    }

    @Test
    public void testFindAllProjectedBy() {
        Page<Cartao> found = cartaoService.findAllProjectedBy(
                "%",
                "%",
                "%",
                1,
                2
        );

        assertThat(found.getContent()).contains(
                cartaoRepository.findById(1L).get(),
                cartaoRepository.findById(2L).get()
        );
    }
}
