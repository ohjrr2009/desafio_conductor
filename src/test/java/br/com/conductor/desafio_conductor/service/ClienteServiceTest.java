package br.com.conductor.desafio_conductor.service;

import br.com.conductor.desafio_conductor.entity.*;
import br.com.conductor.desafio_conductor.entity.Cliente;
import br.com.conductor.desafio_conductor.repository.CartaoRepository;
import br.com.conductor.desafio_conductor.repository.ClienteRepository;
import br.com.conductor.desafio_conductor.service.impl.CartaoServiceImpl;
import br.com.conductor.desafio_conductor.service.impl.ClienteServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;


/**
 * @author Ornélio Hinterholz Junior
 */
@RunWith(SpringRunner.class)
public class ClienteServiceTest {

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @TestConfiguration
    static class ClienteServiceTestContextConfiguration {

        @Bean
        public ClienteService clienteService() {
            return new ClienteServiceImpl();
        }
    }

    @Autowired
    private ClienteService clienteService;

    @MockBean
    private ClienteRepository clienteRepository;

    @MockBean
    RestTemplate restTemplate;

    @Spy
    Logger logger;

    @Before
    public void setUp() throws ParseException {
        Mockito.doNothing().when(logger).info("");
        Mockito.doNothing().when(logger).error("");

        Cliente cliente = new Cliente();
        cliente.setId(1L);
        cliente.setNome("CLIENTE TESTE");
        cliente.setCpf("83762854733");
        cliente.setEndereco("RUA C");
        cliente.setCep("69300000");

        ResponseEntity<Cliente> responseEntity = new ResponseEntity<>(cliente, HttpStatus.OK);

        Mockito.when(restTemplate.postForEntity(any(String.class), any(HttpEntity.class), any(Class.class)))
                .thenReturn(responseEntity);

        Cliente cliente2 = new Cliente();
        cliente2.setId(1L);
        cliente2.setNome("CLIENTE TESTE 2");
        cliente2.setCpf("83762854733");
        cliente2.setEndereco("RUA C");
        cliente2.setCep("69300000");

        List<Cliente> list = Arrays.asList(cliente);
        Page<Cliente> page = new PageImpl<>(list);

        Mockito.when(clienteRepository.findAll())
                .thenReturn(Arrays.asList(cliente, cliente2));

        Mockito.when(clienteRepository.findById(any(Long.class)))
                .thenReturn(Optional.of(cliente));

        Mockito.when(clienteRepository.findAllProjectedBy(
                "%",
                "%",
                PageRequest.of(1, 2)
        )).thenReturn(page);

        Mockito.when(clienteService.findAllProjectedBy(
                "%",
                "%",
                1,
                2
        )).thenReturn(page);

        Mockito.when(clienteRepository.saveAndFlush(any(Cliente.class))).thenReturn(cliente);
    }

    @Test
    public void testFindById() {
        Optional<Cliente> found = clienteService.findById(1L);

        assertThat(found.get()).isEqualTo(
                clienteRepository.findById(1L).get()
        );
    }

    @Test
    public void testFindByAll() {
        Optional<List<Cliente>> found = clienteService.findAll();

        assertThat(found.get()).contains(
                clienteRepository.findById(1L).get(),
                clienteRepository.findById(2L).get()
        );
    }

    @Test
    public void testFindAllProjectedBy() {
        Page<Cliente> found = clienteService.findAllProjectedBy(
                "%",
                "%",
                1,
                2
        );

        assertThat(found.getContent()).contains(
                clienteRepository.findById(1L).get(),
                clienteRepository.findById(2L).get()
        );
    }
}
