package br.com.conductor.desafio_conductor.service;

import br.com.conductor.desafio_conductor.entity.*;
import br.com.conductor.desafio_conductor.entity.Usuario;
import br.com.conductor.desafio_conductor.repository.PessoaRepository;
import br.com.conductor.desafio_conductor.repository.UsuarioRepository;
import br.com.conductor.desafio_conductor.service.impl.PessoaServiceImpl;
import br.com.conductor.desafio_conductor.service.impl.UsuarioServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;


/**
 * @author Ornélio Hinterholz Junior
 */
@RunWith(SpringRunner.class)
public class UsuarioServiceTest {

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @TestConfiguration
    static class UsuarioServiceTestContextConfiguration {

        @Bean
        public UsuarioService usuarioService() {
            return new UsuarioServiceImpl();
        }
    }

    @Autowired
    private UsuarioService usuarioService;

    @MockBean
    private UsuarioRepository usuarioRepository;

    @MockBean
    RestTemplate restTemplate;

    @Spy
    Logger logger;

    @Before
    public void setUp() throws ParseException {
        Mockito.doNothing().when(logger).info("");
        Mockito.doNothing().when(logger).error("");

        Pessoa pessoa = new Pessoa();
        pessoa.setId(1L);
        pessoa.setNome("CLIENTE TESTE");
        pessoa.setCpf("83762854733");
        pessoa.setEndereco("RUA C");
        pessoa.setCep("69300000");

        Usuario usuario = new Usuario();
        usuario.setId(1L);
        usuario.setLogin("teste");
        usuario.setPassword("123");
        usuario.setAtivo(true);
        usuario.setPessoa(pessoa);

        ResponseEntity<Usuario> responseEntity = new ResponseEntity<>(usuario, HttpStatus.OK);

        Mockito.when(restTemplate.postForEntity(any(String.class), any(HttpEntity.class), any(Class.class)))
                .thenReturn(responseEntity);

        Pessoa pessoa2 = new Pessoa();
        pessoa2.setId(1L);
        pessoa2.setNome("CLIENTE TESTE");
        pessoa2.setCpf("83762854733");
        pessoa2.setEndereco("RUA C");
        pessoa2.setCep("69300000");

        Usuario usuario2 = new Usuario();
        usuario2.setId(2L);
        usuario2.setLogin("teste2");
        usuario2.setPassword("123");
        usuario2.setAtivo(true);
        usuario2.setPessoa(pessoa2);

        List<Usuario> list = Arrays.asList(usuario);
        Page<Usuario> page = new PageImpl<>(list);

        Mockito.when(usuarioRepository.findAll())
                .thenReturn(Arrays.asList(usuario, usuario2));

        Mockito.when(usuarioRepository.findById(any(Long.class)))
                .thenReturn(Optional.of(usuario));

        Mockito.when(usuarioRepository.findAllProjectedBy(
                "%",
                Arrays.asList(true),
                "%",
                PageRequest.of(1, 2)
        )).thenReturn(page);

        Mockito.when(usuarioService.findAllProjectedBy(
                "%",
                true,
                "%",
                1,
                2
        )).thenReturn(page);

        Mockito.when(usuarioRepository.saveAndFlush(any(Usuario.class))).thenReturn(usuario);
    }

    @Test
    public void testFindById() {
        Optional<Usuario> found = usuarioService.findById(1L);

        assertThat(found.get()).isEqualTo(
                usuarioRepository.findById(1L).get()
        );
    }

    @Test
    public void testFindByAll() {
        Optional<List<Usuario>> found = usuarioService.findAll();

        assertThat(found.get()).contains(
                usuarioRepository.findById(1L).get(),
                usuarioRepository.findById(2L).get()
        );
    }

    @Test
    public void testFindAllProjectedBy() {
        Page<Usuario> found = usuarioService.findAllProjectedBy(
                "%",
                true,
                "%",
                1,
                2
        );

        assertThat(found.getContent()).contains(
                usuarioRepository.findById(1L).get(),
                usuarioRepository.findById(2L).get()
        );
    }
}
