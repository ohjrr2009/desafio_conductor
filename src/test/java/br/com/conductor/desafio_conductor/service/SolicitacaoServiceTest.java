package br.com.conductor.desafio_conductor.service;

import br.com.conductor.desafio_conductor.entity.*;
import br.com.conductor.desafio_conductor.repository.PessoaRepository;
import br.com.conductor.desafio_conductor.repository.SolicitacaoRepository;
import br.com.conductor.desafio_conductor.service.impl.PessoaServiceImpl;
import br.com.conductor.desafio_conductor.service.impl.SolicitacaoServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;


/**
 * @author Ornélio Hinterholz Junior
 */
@RunWith(SpringRunner.class)
public class SolicitacaoServiceTest {

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @TestConfiguration
    static class SolicitacaoServiceTestContextConfiguration {

        @Bean
        public SolicitacaoService solicitacaoService() {
            return new SolicitacaoServiceImpl();
        }
    }

    @Autowired
    private SolicitacaoService solicitacaoService;

    @MockBean
    private SolicitacaoRepository solicitacaoRepository;

    @MockBean
    RestTemplate restTemplate;

    @Spy
    Logger logger;

    @Before
    public void setUp() throws ParseException {
        Mockito.doNothing().when(logger).info("");
        Mockito.doNothing().when(logger).error("");

        Cliente cliente = new Cliente();
        cliente.setId(1L);
        cliente.setNome("CLIENTE TESTE");
        cliente.setCpf("83762854733");
        cliente.setEndereco("RUA C");
        cliente.setCep("69300000");

        TipoSolicitacao tp = new TipoSolicitacao();
        tp.setId(1L);
        tp.setNome("");
        tp.setDescricao("");

        StatusSolicitacao status = new StatusSolicitacao();
        status.setId(1L);
        tp.setNome("");
        tp.setDescricao("");

        Solicitacao solicitacao = new Solicitacao();
        solicitacao.setId(1L);
        solicitacao.setTipoSolicitacao(tp);
        solicitacao.setCliente(cliente);
        solicitacao.setStatus(status);
        solicitacao.setLimiteCredito(100.0f);

        ResponseEntity<Solicitacao> responseEntity = new ResponseEntity<>(solicitacao, HttpStatus.OK);

        Mockito.when(restTemplate.postForEntity(any(String.class), any(HttpEntity.class), any(Class.class)))
                .thenReturn(responseEntity);

        Cliente cliente2 = new Cliente();
        cliente2.setId(1L);
        cliente2.setNome("CLIENTE TESTE 2");
        cliente2.setCpf("83762854732");
        cliente2.setEndereco("RUA C");
        cliente2.setCep("69300000");

        TipoSolicitacao tp2 = new TipoSolicitacao();
        tp2.setId(1L);
        tp2.setNome("");
        tp2.setDescricao("");

        StatusSolicitacao status2 = new StatusSolicitacao();
        status2.setId(1L);
        status2.setNome("");
        status2.setDescricao("");

        Solicitacao solicitacao2 = new Solicitacao();
        solicitacao2.setId(1L);
        solicitacao2.setTipoSolicitacao(tp);
        solicitacao2.setCliente(cliente);
        solicitacao2.setStatus(status);
        solicitacao2.setLimiteCredito(100.0f);

        List<Solicitacao> list = Arrays.asList(solicitacao);
        Page<Solicitacao> page = new PageImpl<>(list);

        Mockito.when(solicitacaoRepository.findAll())
                .thenReturn(Arrays.asList(solicitacao, solicitacao2));

        Mockito.when(solicitacaoRepository.findById(any(Long.class)))
                .thenReturn(Optional.of(solicitacao));

        Mockito.when(solicitacaoRepository.findAllProjectedBy(
                "%",
                "%",
                "%",
                PageRequest.of(1, 2)
        )).thenReturn(page);

        Mockito.when(solicitacaoService.findAllProjectedBy(
                "%",
                "%",
                "%",
                1,
                2
        )).thenReturn(page);

        Mockito.when(solicitacaoRepository.saveAndFlush(any(Solicitacao.class))).thenReturn(solicitacao);
    }

    @Test
    public void testFindById() {
        Optional<Solicitacao> found = solicitacaoService.findById(1L);

        assertThat(found.get()).isEqualTo(
                solicitacaoRepository.findById(1L).get()
        );
    }

    @Test
    public void testFindByAll() {
        Optional<List<Solicitacao>> found = solicitacaoService.findAll();

        assertThat(found.get()).contains(
                solicitacaoRepository.findById(1L).get(),
                solicitacaoRepository.findById(2L).get()
        );
    }

    @Test
    public void testFindAllProjectedBy() {
        Page<Solicitacao> found = solicitacaoService.findAllProjectedBy(
                "%",
                "%",
                "%",
                1,
                2
        );

        assertThat(found.getContent()).contains(
                solicitacaoRepository.findById(1L).get(),
                solicitacaoRepository.findById(2L).get()
        );
    }
}
