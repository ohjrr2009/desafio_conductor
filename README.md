# Análise de Crédito - API BACK

Esta API tem como objetivo prover endpoints REST para consumo pelas aplicações Front-end na Intranet operados pelos Analistas Financeiros e Atendentes de Linha de Frente (Operadores), ambos no Back office.

Além disso, a API provê também uma lista dinâmica de possíveis pedidos de aprovação de emissão de cartões, bem como pedidos de aumento de crédito nos limites dos mesmos. Os principais filtros relacionados ao contexto são disponibilizados para a realização das consultas, ressalta-se que os filtros são todos opcionais e todos os retornos de listas independentemente da entidade referenciada são paginadas de forma personalizável.

Embora a API já esteja preparada para a execução dos fluxos de emissão de cartão de crédito e o fluxo de análise de aumento de crédito. Os endpoints automatizados para consecução dos resultados dos fluxos não foram criados por insuficiência de tempo hábil. No entanto, salienta-se que na massa de dados carregada na inicialização da aplicação já trás tais resultados consolidados, muito embora a execução seja manual.

# Como Funciona (Business)

Basicamente o processo de autorização para emissão de cartão de crédito, bem como o processo para aumento do crédito estabelecido para o referido cartão pela Instituição Financeira (IF), funcionam seguindo a lógica de mudança nos estados das solicitações cadastradas no sistema.

Uma vez que o cliente cadastra-se no sistema, anexando todos os seus documentos para análise _a posteriori_, ele pode realizar a criação de solicitações. Caso o mesmo não possua um cartão de crédito emitido pela IF, a primeira solicitação disponível será o **Pedido de Novo Cartão de Crédito**.

Tal solicitação cai na fila de solicitações do sistema com o status **GNCC_PENDENTE** (Pendente de Aprovação de Geração de Novo Cartão de Crédito). O **OPERADOR** que realiza o atendimento de linha de frente com o cliente, deve então realizar a conferência das informações cadastradas, juntamente com a conferência dos documentos digitalizados enviados, seguindo todas as diretrizes de compliance de **KYC** e **PLD**. Uma vez que todos os critérios sejam atendidos o **OPERADOR** realiza então a respectiva aprovação para a emissão do cartão de crédito ao novo cliente da IF. Para tanto, o **OPERADOR** modifica o status da solicitação para **GNCC_APROVADO** (Aprovação de Geração de Novo Cartão de Crédito) e a solicitação é encerrada. Caso haja alguma inconsistência nas informações prestadas pelo cliente, o **OPERADOR** deve rejeitar o pedido alterando o status da solicitação para **GNCC_REJEITADO** (Rejeição de Geração de Novo Cartão de Crédito) e a solicitação é encerrada.

O cliente com um cartão emitido pela IF, inicia seu uso com um limite padrão de R$ 100,00 (cem reais). O cliente, caso deseje, pode então criar uma nova solicitação solicitando aumento de limite de crédito, esse tipo de solicitação será disponibilizada ao cliente com o nome **Pedido de Aumento de Limite de Cartão de Crédito)**, o cliente no envio da solicitação apenas precisa passar o limite desejado para a criação da solicitação.

O Analista Financeiro, doravante **ANALISTA**, averigua na fila de solicitações do sistema as que possuem o status **ALCC_PENDENTE** (Pendente de Aumento de Limite de Cartão de Crédito). Após o incício do atendimento da solicitação, o **ANALISTA** verifica tal pedido conforme as normativas de análise de aumento de crédito da IF. Caso o cliente enquadre-se dentro dos critérios estebelecidos, ele poderá ter sua solicitação atendida e consequentemente seu limite de crédito para o respectivo cartão de crédito aumentado. No caso de aprovação o **ANALISTA** altera o status da solicitação para **ALCC_APROVADO** (Aprovação de Aumento de Limite de Cartão de Crédito) e encerra a solicitação. Tal movimento da solicitação já deve automaticamente atualizar o limite de crédito anteriormente estabelecido no cartão de crédito do cliente. Caso o cliente não se enquadre nos critérios, o **ANALISTA** rejeita a solicitação alterando o status **ALCC_REJEITADO** (Rejeição de Aumento de Limite de Cartão de Crédito) e encerra a solicitação.

# Como usar (Technical)

A aplicação pode ser acessada a partir da chamada http://localhost:8089/login, um formulário será exibido, conforme a figura abaixo. Ressalta-se que o intuito da restrição de acesso (mesmo que na Intranet é não permitir que pessoas sem a devida permissão tenha conhecimento da estrutura de chamadas de endpoints).

![...imagem...](tela_login.png)

Os **_usuários/senhas_** para acesso foram cadastrados _hard-coded_ com uso do recurso de autenticação em memória do Spring Security e são os seguintes:
 - admin/admin - possui acesso à todos os recursos
 - operario/operario - (embora tenha sido criado, esse usuário tem permissões ainda não liberadas para manipulação dos endpoints)
 - analista/analista - (embora tenha sido criado, esse usuário tem permissões ainda não liberadas para manipulação dos endpoints)

Uma vez que um usuário tenha realizado o seu login satisfatoriamente, a sua sessão expira em 30 minutos. Como não foi implementada a funcionalidade de refresh automático da sessão, após a expiração da mesma, o usuário deve realizar o logout manualmente para limpar o cache executando no navegador a url http://localhost:8089/logout.

Após o login, caso o usuário seja o _admin_ o mesmo será redirecionado a url http://localhost:8089/swagger-ui.html. Que disponibiliza o Swagger de todos os endpoints criados na API.
      
![...imagem...](endpoints.png)

Salienta-se que chamadas aos endpoints também podem ser executadas via Postman ou outro mecanismo que use curl ou afins, desde que a sessão esteja ativa na máquina.

Para a inicialização da API basta, uma vez que o build tenha sido realizado com sucesso, a execução do comando quando estando na raíz do projeto:

`java -jar target\desafio_conductor-0.0.1-SNAPSHOT.jar`

# Configurações

As configurações estão presentes no arquivo `application.properties`, localizado no caminho `src/main/resources` a partir da raíz do projeto, e são carregadas na inicialização da aplicação.

O perfil ativo da aplicação está previamente configurado para **DEV**.

A porta padrão utilizada pela aplicação é **8089**, podendo ser modificada alterando o referido arquivo de configuração. 

A massa inicial de dados é carregada automaticamente para um banco de dados denominado `credcard` (que deve ter sido previamente criado no PostgreSQL com os devidos ajustes, caso necessários, realizados no arquivo `application.properties`). Tal massa de dados é carregada pelo Spring Boot na inicialização do servidor a partir do arquivo `data.sql` localizado no caminho `src/main/resources` a partir da raíz do projeto.

Em tal massa de dados, é realizada a criação de Perfis, Tipos de Solicitações, Status de Solicitações, estes sendo registros considerados estáticos para a aplicação. Além disso, também são populadas com alguns registros, as tabelas de Pessoa, Usuário, Cliente, Solicitação e Cartão.

O **Diagrama Entidade-Relacionamento** (DER) do modelo de dados gerado via ORM pode ser visto abaixo:

![...imagem...](der.png)

Muito embora a massa inicial de dados seja automaticamente carregada, foi disponibilizado um dump do banco de dados que pode ser importado por meio do arquivo `dump_credcard.backup` localizado na raíz do projeto.

# Health Check

A aplicação faz uso do **Spring Boot Actuator**, de forma que o mesmo provê alguns endpoints úteis para monitoração do estado do ambiente em execução.

Dois dos endpoints providos estão liberados para acesso público e a chamada pode ser testada.
 - http://localhost:8089/health
   Exemplo de Response:
   ```json
   {
       "status": "UP"
   }
   ```

 - http://localhost:8089/info
   Exemplo de Response:
   ```json
    {
      "build": {
        "artifact": "desafio_conductor",
        "name": "desafio_conductor",
        "time": "2021-02-16T12:41:30.044Z",
        "version": "0.0.1-SNAPSHOT",
        "group": "br.com.conductor"
      }
    }
   ```

# Testes

Foram desenvolvidos Testes Unitários com Junit/Mockito que abrangem os Controllers e Services da API.

Para a análise de qualidade e cobertura de código efetuada pelos testes foi utilizado o Jacoco que está configurado no POM da API e para a exibição dos resultados o SonarQube foi usado.

![...imagem...](sonar.png)

Como pode ser visto a qualidade da API entregue foi satisfatória, embora exija alguns ajustes principalmente no sentido de remoção de Code Smells e Vulnerabilidades.

# Tech/framework utilizados

## Construído com

-   Spring Boot 2.4.2
-   Spring Security 2.4.2
-   JPA 2.4.2
-   Lombok 1.18.16
-   SpringFox Swagger2 2.9.2
-   JUnit Jupiter 5.7.0
-   Jacoco 0.8.6
-   Mockito 3.6.0
-   Java 11
-   Maven
-   PostgreSQL 42.2.18
